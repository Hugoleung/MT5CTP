//+------------------------------------------------------------------+
//|                                                        Trade.mqh |
//+------------------------------------------------------------------+
#property copyright     "Copyright 2020,Guorui Holding (Shandong) Co.,Ltd."
#property version       "1.50"
#property link          ""
#property description   "本程序仅用于量化投资爱好者测试、研究。程序使用者承诺："
#property description   "1.本人/本单位熟知本程序的适用范围，自愿承担因商业应用造成的违法、违规、侵权责任。"
#property description   "2.本人/本单位熟知金融衍生品的各种交易及代理风险，并自愿承担损失及违法、违规、违约责任。"
#property description   "3.本人/本单位熟知计算机程序的各种固有风险，并自愿承担损失及违法、违规、违约责任。"
#property description   "4.本人/本单位熟知金融监管规则，依法依规参与金融市场，并自愿承担损失及违法、违规责任。"
#property description   "5.本人/本单位认可开始使用本程序，视为同意并做出上述承诺，停用本程序后，上述承诺依然有效。"
//+------------------------------------------------------------------+
//| include 外部依赖                                                 |
//+------------------------------------------------------------------+
#include <Object.mqh>
#include "mt5ctp.mqh"
#include "OrderInfo.mqh"
#include "HistoryOrderInfo.mqh"
#include "PositionInfo.mqh"
#include "DealInfo.mqh"
#include "SymbolInfo.mqh"
#include "AccountInfo.mqh"
//+------------------------------------------------------------------+
//| Class CTrade.                                                    |
//| Appointment: Class trade operations.                             |
//|              Derives from class CObject.                         |
//+------------------------------------------------------------------+
class CTrade : public CObject
  {
protected:
   MqlTradeRequest   m_request;              // request data
   MqlTradeResult    m_result;               // result data
   MqlTradeCheckResult m_check_result;       // result check data
   ulong             m_deviation;            // 默认滑点
   ENUM_ORDER_TYPE_FILLING m_type_filling;   // 报单特殊指令/FAK/FOK/默认RETURN(普通)
   COrderInfo        m_order;                // 工作订单对象
   CPositionInfo     m_position;             // 持仓对象
   CSymbolInfo       m_symbol;               // 合约对象
   CAccountInfo      m_account;              // 账户对象
   CHistoryOrderInfo m_history_order;        // 历史订单对象
   CDealInfo         m_deal;                 // 成交对象
public:
                     CTrade(void);
                    ~CTrade(void);
   //--- methods of access to protected data
   void              Request(MqlTradeRequest &request) const;
   void              Result(MqlTradeResult &result) const;
   void              CheckResult(MqlTradeCheckResult &check_result) const;
   //--- trade methods
   // 设置滑点的tick点数
   void              SetDeviationInPoints(const ulong deviation) { m_deviation=deviation;            }
   // 设置特殊指令类型
   void              SetTypeFilling(const ENUM_ORDER_TYPE_FILLING filling) { m_type_filling=filling; }
   // 数据重置/恢复默认
   void              Reset(void);
   //--- methods for working with positions
   // 开仓
   bool              PositionOpen(const string symbol,const ENUM_ORDER_TYPE order_type,const long volume,
                                  const double price,const ulong deviation=ULONG_MAX,const double sl=0.0,const double tp=0.0);
   // 平仓
   bool              PositionClose(const string symbol,const ENUM_ORDER_TYPE order_type,const long volume,
                                   const double price,const long close_type = ORDER_TYPE_EXIT,const ulong deviation=ULONG_MAX);
   // 选中/快捷平仓/ticket
   bool              PositionClose(const string ticket,const long volume,const double price,const ulong deviation=ULONG_MAX);
   // 选中/快捷平仓/index
   bool              PositionClose(const int pos,const long volume,const double price,const ulong deviation=ULONG_MAX);
   // 修改止损止盈/ticket
   bool              PositionModify(const string ticket,const double sl,const double tp);
   // 修改止损止盈/index
   bool              PositionModify(const int pos,const double sl,const double tp);
   // 自动开平/先平今/后平昨/再开仓
   bool              PositionAuto(const string symbol,const ENUM_ORDER_TYPE order_type,const long volume,const double price,const ulong deviation=ULONG_MAX);
   //--- methods for working with pending orders
   // 报单/挂单
   bool              OrderOpen(const string symbol,const ENUM_ORDER_TYPE order_type,const long volume,
                               const double price,const double stoplimit_price,const double sl,const double tp);
   // 报单/挂单/修改止损止盈
   bool              OrderModify(const string ticket,const double sl,const double tp);
   bool              OrderModify(const int order,const double sl,const double tp);
   // 报单/挂单/修改价格数量
   bool              OrderModify(const string ticket,const long volume,const double price,const double stoplimit_price);
   bool              OrderModify(const int order,const long volume,const double price,const double stoplimit_price);
   bool              OrderModify(const string ticket,const long volume,const double price,const double stoplimit_price,const double sl,const double tp);
   bool              OrderModify(const int order,const long volume,const double price,const double stoplimit_price,const double sl,const double tp);
   // 撤单
   bool              OrderDelete(const string ticket);
   bool              OrderDelete(const int order);
   //--- additions methods /open
   bool              Buy(const string symbol,const long volume,double price,const double sl=0.0,const double tp=0.0);
   bool              Sell(const string symbol,const long volume,double price,const double sl=0.0,const double tp=0.0);
   bool              BuyLimit(const string symbol,const long volume,const double price,const double sl=0.0,const double tp=0.0);
   bool              BuyStop(const string symbol,const long volume,const double price,const double sl=0.0,const double tp=0.0);
   bool              SellLimit(const string symbol,const long volume,const double price,const double sl=0.0,const double tp=0.0);
   bool              SellStop(const string symbol,const long volume,const double price,const double sl=0.0,const double tp=0.0);
   //---
   virtual bool      OrderCheck(MqlTradeRequest &request,MqlTradeCheckResult &check_result);
   virtual bool      OrderSend(MqlTradeRequest &request,MqlTradeResult &result);
   //--- methods for working with event
   string            ExchangeID(long lparam);
   string            OrderRef(string sparam);
   string            OrderSysID(string sparam);
   string            TradeID(double dparam);
   long              FrontID(long lparam);
   long              SessionID(double dparam);
   string            OrderTicket(const long lparam,const double dparam,const string sparam);
   string            DealTicket(const long lparam,const double dparam,const string sparam);
   virtual void      OnChartEvent(const int id,const long &lparam,const double &dparam,const string &sparam);
   virtual void      OnOrder(const int id,const long &lparam,const double &dparam,const string &sparam) {};
   virtual void      OnTrade(const int id,const long &lparam,const double &dparam,const string &sparam) {};
   virtual void      OnError(const int id,const long &lparam,const double &dparam,const string &sparam) {};
protected:
   // 清除数据记录
   void              ClearStructures(void);
   // 检查是否工作状态
   bool              IsStopped(const string function);
  };
//+------------------------------------------------------------------+
//| Constructor                                                      |
//+------------------------------------------------------------------+
CTrade::CTrade(void) : m_deviation(5),
   m_type_filling(ORDER_FILLING_RETURN)
  {
//--- initialize protected data
   ClearStructures();
  }
//+------------------------------------------------------------------+
//| Destructor                                                       |
//+------------------------------------------------------------------+
CTrade::~CTrade(void)
  {
  }
//+------------------------------------------------------------------+
//| Get the request structure                                        |
//+------------------------------------------------------------------+
void CTrade::Request(MqlTradeRequest &request) const
  {
   request.action      =m_request.action;
   request.magic       =m_request.magic;
   request.order       =m_request.order;
   request.symbol      =m_request.symbol;
   request.volume      =m_request.volume;
   request.price       =m_request.price;
   request.stoplimit   =m_request.stoplimit;
   request.sl          =m_request.sl;
   request.tp          =m_request.tp;
   request.deviation   =m_request.deviation;
   request.type        =m_request.type;
   request.type_filling=m_request.type_filling;
   request.type_time   =m_request.type_time;
   request.expiration  =m_request.expiration;
   request.comment     =m_request.comment;
   request.position    =m_request.position;
   request.position_by =m_request.position_by;
  }
//+------------------------------------------------------------------+
//| Get the result structure                                         |
//+------------------------------------------------------------------+
void CTrade::Result(MqlTradeResult &result) const
  {
   result.retcode   =m_result.retcode;
   result.deal      =m_result.deal;
   result.order     =m_result.order;
   result.volume    =m_result.volume;
   result.price     =m_result.price;
   result.bid       =m_result.bid;
   result.ask       =m_result.ask;
   result.comment   =m_result.comment;
   result.request_id=m_result.request_id;
   result.retcode_external=m_result.retcode_external;
  }
//+------------------------------------------------------------------+
//| Get the check result structure                                   |
//+------------------------------------------------------------------+
void CTrade::CheckResult(MqlTradeCheckResult &check_result) const
  {
//--- copy structure
   check_result.retcode     =m_check_result.retcode;
   check_result.balance     =m_check_result.balance;
   check_result.equity      =m_check_result.equity;
   check_result.profit      =m_check_result.profit;
   check_result.margin      =m_check_result.margin;
   check_result.margin_free =m_check_result.margin_free;
   check_result.margin_level=m_check_result.margin_level;
   check_result.comment     =m_check_result.comment;
  }
//+------------------------------------------------------------------+
//| trade methods                                                    |
//+------------------------------------------------------------------+
void CTrade::Reset(void)
  {
//--- initialize protected data
   ClearStructures();
   m_deviation = 5;
   m_type_filling = ORDER_FILLING_RETURN;
  }

//+------------------------------------------------------------------+
//| methods for working with positions                               |
//+------------------------------------------------------------------+
// 开仓
bool CTrade::PositionOpen(const string symbol,const ENUM_ORDER_TYPE order_type,const long volume,
                          const double price,const ulong deviation=ULONG_MAX,const double sl=0.0,const double tp=0.0)
  {
//--- check stopped
   if(IsStopped(__FUNCTION__))
     {
      return(false);
     }
//--- 账户状态检查
   if(!m_account.AccountExists())
     {
      return(false);
     }
//--- 合约/状态检查
   string order_symbol_trade = symbol;
   if(symbol==NULL || ::StringLen(symbol)==0)
     {
      order_symbol_trade = _Symbol;
     }
   if(!m_symbol.Select(order_symbol_trade))
     {
      return(false);
     }
   else
     {
      if(!m_symbol.SymbolExists())
        {
         return(false);
        }
     }
//--- 参数检查
   if(!(order_type==ORDER_TYPE_BUY || order_type==ORDER_TYPE_SELL))
     {
      m_result.retcode=TRADE_RETCODE_INVALID;
      m_result.comment="Invalid order type";
      return(false);
     }
   if(volume<=0)
     {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      m_result.comment="Invalid order volume";
      return(false);
     }
//--- clean
   ClearStructures();
//--- setting request
   m_request.action        = TRADE_ACTION_DEAL;
   m_request.symbol        = order_symbol_trade;
   m_request.volume        = (double)volume;
   m_request.type          = order_type;
   m_request.price         = price;
   m_request.sl            = sl;
   m_request.tp            = tp;
   m_request.deviation     = (deviation==ULONG_MAX)?m_deviation:deviation;
   m_request.type_filling  = m_type_filling;
//--- action and return the result
   return(CTP::OrderSend(m_request,m_result));
  }
// 平仓
bool CTrade::PositionClose(const string symbol,const ENUM_ORDER_TYPE order_type,const long volume,
                           const double price,const long close_type = ORDER_TYPE_EXIT,const ulong deviation=ULONG_MAX)
  {
//--- check stopped
   if(IsStopped(__FUNCTION__))
     {
      return(false);
     }
//--- 账户状态检查
   if(!m_account.AccountExists())
     {
      return(false);
     }
//--- 合约/状态检查
   string order_symbol_trade = symbol;
   if(symbol==NULL || ::StringLen(symbol)==0)
     {
      order_symbol_trade = _Symbol;
     }
   if(!m_symbol.Select(order_symbol_trade))
     {
      return(false);
     }
   else
     {
      if(!m_symbol.SymbolExists())
        {
         return(false);
        }
     }
//--- 参数检查
   if(!(order_type==ORDER_TYPE_BUY || order_type==ORDER_TYPE_SELL))
     {
      m_result.retcode=TRADE_RETCODE_INVALID;
      m_result.comment="Invalid order type";
      return(false);
     }
   if(close_type>=0)
     {
      m_result.retcode=TRADE_RETCODE_INVALID;
      m_result.comment="Invalid order close type";
      return(false);
     }
   if(volume<=0)
     {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      m_result.comment="Invalid order close volume";
      return(false);
     }
//--- clean
   ClearStructures();
//--- setting request
   m_request.action        = TRADE_ACTION_DEAL;
   m_request.symbol        = order_symbol_trade;
   m_request.volume        = (double)volume;
   m_request.type          = order_type;
   m_request.price         = price;
   m_request.deviation     = (deviation==ULONG_MAX)?m_deviation:deviation;
   m_request.type_filling  = m_type_filling;
   m_request.position      = close_type;
//--- action and return the result
   return(CTP::OrderSend(m_request,m_result));
  }
// 选中/快捷平仓/ticket
bool CTrade::PositionClose(const string ticket,const long volume,const double price,const ulong deviation=ULONG_MAX)
  {
// 选中持仓
   if(!m_position.Select(ticket))
     {
      return(false);
     }
//--- 参数检查
   long order_lots = volume;
   if(order_lots==0)
     {
      order_lots = m_position.Position();
     }
   ENUM_ORDER_TYPE order_type = m_position.PositionType()==POSITION_TYPE_BUY ? ORDER_TYPE_SELL : ORDER_TYPE_BUY;
   long close_type = m_position.PositionDate()=='2'?ORDER_TYPE_EXIT:ORDER_TYPE_EXITTODAY;
   return(PositionClose(m_position.Symbol(),order_type,order_lots,price,close_type,deviation));
  }
// 选中/快捷平仓/index
bool CTrade::PositionClose(const int pos,const long volume,const double price,const ulong deviation=ULONG_MAX)
  {
   if(!m_position.SelectByIndex(pos))
     {
      return(false);
     }
//--- 参数检查
   long order_lots = volume;
   if(order_lots==0)
     {
      order_lots = m_position.Position();
     }
   ENUM_ORDER_TYPE order_type = m_position.PositionType()==POSITION_TYPE_BUY ? ORDER_TYPE_SELL : ORDER_TYPE_BUY;
   long close_type = m_position.PositionDate()=='2'?ORDER_TYPE_EXIT:ORDER_TYPE_EXITTODAY;
   return(PositionClose(m_position.Symbol(),order_type,order_lots,price,close_type,deviation));
  }
// 修改止损止盈/ticket
bool CTrade::PositionModify(const string ticket,const double sl,const double tp)
  {
   if(!m_position.Select(ticket))
     {
      return(false);
     }
   return(PositionModify(m_position.Index(),sl,tp));
  }
// 修改止损止盈/index
bool CTrade::PositionModify(const int pos,const double sl,const double tp)
  {
//--- check stopped
   if(IsStopped(__FUNCTION__))
     {
      return(false);
     }
//--- 账户状态检查
   if(!m_account.AccountExists())
     {
      return(false);
     }
   if(!m_position.SelectByIndex(pos))
     {
      return(false);
     }
   if(m_position.Position()<=0)
     {
      return(false);
     }
//--- 合约/状态检查
   if(!m_symbol.Select(m_position.Symbol()))
     {
      return(false);
     }
//--- clean
   ClearStructures();
//--- setting request
   m_request.action        = TRADE_ACTION_SLTP;
   m_request.symbol        = m_position.Symbol();
   m_request.position      = pos;
   m_request.sl            = sl;
   m_request.tp            = tp;
//--- action and return the result
   return(CTP::OrderSend(m_request,m_result));
  }
// 自动开平/先平今/后平昨/再开仓
bool CTrade::PositionAuto(const string symbol,const ENUM_ORDER_TYPE order_type,const long volume,const double price,const ulong deviation=ULONG_MAX)
  {
//--- check stopped
   if(IsStopped(__FUNCTION__))
     {
      return(false);
     }
//--- 账户状态检查
   if(!m_account.AccountExists())
     {
      return(false);
     }
//--- 合约/状态检查
   string order_symbol_trade = symbol;
   if(symbol==NULL || ::StringLen(symbol)==0)
     {
      order_symbol_trade = _Symbol;
     }
   if(!m_symbol.Select(order_symbol_trade))
     {
      return(false);
     }
   else
     {
      if(!m_symbol.SymbolExists())
        {
         return(false);
        }
     }
//--- 参数检查
   if(!(order_type==ORDER_TYPE_BUY || order_type==ORDER_TYPE_SELL))
     {
      m_result.retcode=TRADE_RETCODE_INVALID;
      m_result.comment="Invalid order type";
      return(false);
     }
   if(volume<=0)
     {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      m_result.comment="Invalid order volume";
      return(false);
     }
//---持仓检查
   long pos_volume_today = 0; // 今仓数量
   long pos_volume = 0;       // 昨仓数量
   ENUM_POSITION_TYPE check_pos_type = order_type==ORDER_TYPE_BUY?POSITION_TYPE_SELL:POSITION_TYPE_BUY;
   int pos_total = m_position.ToTal();
   for(int index = 1; index<=pos_total; index++)
     {
      if(!m_position.SelectByIndex(index))
        {
         continue;
        }
      if(m_position.Symbol()!=order_symbol_trade)
        {
         continue;
        }
      if(m_position.PositionType()==check_pos_type)
        {
         if(m_position.PositionDate()=='1')
           {
            pos_volume_today += m_position.Position();
           }
         else
            if(m_position.PositionDate()=='2')
              {
               pos_volume += m_position.Position();
              }
        }
     }
// 报单数量大于今仓数量/先平今仓
   bool res = false;
   long check_volume_today = volume - pos_volume_today;
   if(check_volume_today>0)
     {
      //今仓全平
      if(pos_volume_today>0)
        {
         res = PositionClose(order_symbol_trade,order_type,pos_volume_today,price,ORDER_TYPE_EXITTODAY,deviation);
        }
      //检查昨仓
      long check_volume = check_volume_today - pos_volume;
      if(check_volume>0)
        {
         // 昨仓全平
         if(pos_volume>0)
           {
            res = PositionClose(order_symbol_trade,order_type,pos_volume,price,ORDER_TYPE_EXIT,deviation);
           }
         // 余仓开仓
         res = PositionOpen(order_symbol_trade,order_type,check_volume,price,deviation);
        }
      else
        {
         // 昨仓足够/平昨仓
         res = PositionClose(order_symbol_trade,order_type,check_volume_today,price,ORDER_TYPE_EXIT,deviation);
        }
     }
   else
     {
      // 今仓足够/平今仓
      res = PositionClose(order_symbol_trade,order_type,volume,price,ORDER_TYPE_EXITTODAY,deviation);
     }
   return(res);
  }
//+------------------------------------------------------------------+
//| methods for working with pending orders                          |
//+------------------------------------------------------------------+
// 报单/挂单
bool CTrade::OrderOpen(const string symbol,const ENUM_ORDER_TYPE order_type,const long volume,
                       const double price,const double stoplimit_price,const double sl,const double tp)
  {
//--- check stopped
   if(IsStopped(__FUNCTION__))
     {
      return(false);
     }
//--- 账户状态检查
   if(!m_account.AccountExists())
     {
      return(false);
     }
//--- 合约/状态检查
   string order_symbol_trade = symbol;
   if(symbol==NULL || ::StringLen(symbol)==0)
     {
      order_symbol_trade = _Symbol;
     }
   if(!m_symbol.Select(order_symbol_trade))
     {
      return(false);
     }
   else
     {
      if(!m_symbol.SymbolExists())
        {
         return(false);
        }
     }
//--- 参数检查
   if(order_type==ORDER_TYPE_BUY || order_type==ORDER_TYPE_SELL)
     {
      m_result.retcode=TRADE_RETCODE_INVALID;
      m_result.comment="Invalid order type";
      return(false);
     }
   if(volume<=0)
     {
      m_result.retcode=TRADE_RETCODE_INVALID_VOLUME;
      m_result.comment="Invalid order volume";
      return(false);
     }
   if(price<=0.0)
     {
      m_result.retcode=TRADE_RETCODE_INVALID_PRICE;
      m_result.comment="Invalid order price";
      return(false);
     }
   if(stoplimit_price<=0.0)
     {
      m_result.retcode=TRADE_RETCODE_INVALID_STOPS;
      m_result.comment="Invalid order stop/limit price";
      return(false);
     }
//--- clean
   ClearStructures();
//--- setting request
   m_request.action      = TRADE_ACTION_PENDING;
   m_request.symbol      = order_symbol_trade;
   m_request.volume      = (double)volume;
   m_request.type        = order_type;
   m_request.stoplimit   = stoplimit_price;
   m_request.price       = price;
   m_request.sl          = sl;
   m_request.tp          = tp;
   m_request.type_filling  = m_type_filling;
//--- action and return the result
   return(CTP::OrderSend(m_request,m_result));
  }
// 报单/挂单/修改止损止盈
bool CTrade::OrderModify(const string ticket,const double sl,const double tp)
  {
//--- 订单选择
   if(!m_order.Select(ticket))
     {
      return(false);
     }
   return(OrderModify(m_order.Index(),m_order.Volume(),m_order.LimitPrice(),m_order.StopPrice(),sl,tp));
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CTrade::OrderModify(const int order,const double sl,const double tp)
  {
//--- 订单选择
   if(!m_order.SelectByIndex(order))
     {
      return(false);
     }
   return(OrderModify(order,m_order.Volume(),m_order.LimitPrice(),m_order.StopPrice(),sl,tp));
  }
// 报单/挂单/修改价格数量
bool CTrade::OrderModify(const string ticket,const long volume,const double price,const double stoplimit_price)
  {
//--- 订单选择
   if(!m_order.Select(ticket))
     {
      return(false);
     }
   return(OrderModify(m_order.Index(),volume,price,stoplimit_price,m_order.StopLoss(),m_order.TakeProfit()));
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CTrade::OrderModify(const int order,const long volume,const double price,const double stoplimit_price)
  {
//--- 订单选择
   if(!m_order.SelectByIndex(order))
     {
      return(false);
     }
   return(OrderModify(order,volume,price,stoplimit_price,m_order.StopLoss(),m_order.TakeProfit()));
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CTrade::OrderModify(const string ticket,const long volume,const double price,const double stoplimit_price,const double sl,const double tp)
  {
//--- 订单选择
   if(!m_order.Select(ticket))
     {
      return(false);
     }
   return(OrderModify(m_order.Index(),volume,price,stoplimit_price,sl,tp));
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CTrade::OrderModify(const int order,const long volume,const double price,const double stoplimit_price,const double sl,const double tp)
  {
//--- check stopped
   if(IsStopped(__FUNCTION__))
     {
      return(false);
     }
//--- 账户状态检查
   if(!m_account.AccountExists())
     {
      return(false);
     }
//--- 取得持仓
   if(!m_order.SelectByIndex(order))
     {
      return(false);
     }
//--- 合约/状态检查
   if(!m_symbol.Select(m_order.Symbol()))
     {
      return(false);
     }
   else
     {
      if(!m_symbol.SymbolExists())
        {
         return(false);
        }
     }
//--- clean
   ClearStructures();
//--- setting request
   m_request.action      = TRADE_ACTION_MODIFY;
   m_request.symbol      = m_order.Symbol();
   m_request.order       = order;
   m_request.volume      = (double)volume;
   m_request.stoplimit   = stoplimit_price;
   m_request.price       = price;
   m_request.sl          = sl;
   m_request.tp          = tp;
   m_request.type_filling  = m_type_filling;
//--- action and return the result
   return(CTP::OrderSend(m_request,m_result));
  }
// 撤单
bool CTrade::OrderDelete(const string ticket)
  {
//--- 取得持仓
   if(!m_order.Select(ticket))
     {
      return(false);
     }
   return(OrderDelete(m_order.Index()));
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CTrade::OrderDelete(const int order)
  {
//--- check stopped
   if(IsStopped(__FUNCTION__))
     {
      return(false);
     }
//--- 账户状态检查
   if(!m_account.AccountExists())
     {
      return(false);
     }
//--- 取得持仓
   if(!m_order.SelectByIndex(order))
     {
      return(false);
     }
//--- 合约/状态检查
   if(!m_symbol.Select(m_order.Symbol()))
     {
      return(false);
     }
   else
     {
      if(!m_symbol.SymbolExists())
        {
         return(false);
        }
     }
//--- clean
   ClearStructures();
//--- setting request
   m_request.action      = TRADE_ACTION_REMOVE;
   m_request.symbol      = m_order.Symbol();
   m_request.order       = order;
//--- action and return the result
   return(CTP::OrderSend(m_request,m_result));
  }
//+------------------------------------------------------------------+
//| additions methods /open                                          |
//+------------------------------------------------------------------+
bool CTrade::Buy(const string symbol,const long volume,double price,const double sl=0.0,const double tp=0.0)
  {
   return(PositionOpen(symbol,ORDER_TYPE_BUY,volume,price,ULONG_MAX,sl,tp));
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CTrade::Sell(const string symbol,const long volume,double price,const double sl=0.0,const double tp=0.0)
  {
   return(PositionOpen(symbol,ORDER_TYPE_SELL,volume,price,ULONG_MAX,sl,tp));
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CTrade::BuyLimit(const string symbol,const long volume,const double price,const double sl=0.0,const double tp=0.0)
  {
   return(OrderOpen(symbol,ORDER_TYPE_BUY_LIMIT,volume,0.0,price,sl,tp));
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CTrade::BuyStop(const string symbol,const long volume,const double price,const double sl=0.0,const double tp=0.0)
  {
   return(OrderOpen(symbol,ORDER_TYPE_BUY_STOP,volume,0.0,price,sl,tp));
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CTrade::SellLimit(const string symbol,const long volume,const double price,const double sl=0.0,const double tp=0.0)
  {
   return(OrderOpen(symbol,ORDER_TYPE_SELL_LIMIT,volume,0.0,price,sl,tp));
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CTrade::SellStop(const string symbol,const long volume,const double price,const double sl=0.0,const double tp=0.0)
  {
   return(OrderOpen(symbol,ORDER_TYPE_SELL_STOP,volume,0.0,price,sl,tp));
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CTrade::OrderCheck(MqlTradeRequest &request,MqlTradeCheckResult &check_result)
  {
   ::ZeroMemory(m_request);
   ::ZeroMemory(m_check_result);
   m_request.action        = request.action;
   m_request.symbol        = request.symbol;
   m_request.magic         = request.magic;
   m_request.order         = request.order;
   m_request.volume        = request.volume;
   m_request.price         = request.price;
   m_request.stoplimit     = request.stoplimit;
   m_request.sl            = request.sl;
   m_request.tp            = request.tp;
   m_request.deviation     = request.deviation;
   m_request.type          = request.type;
   m_request.type_filling  = request.type_filling;
   m_request.type_time     = request.type_time;
   m_request.expiration    = request.expiration;
   m_request.comment       = request.comment;
   m_request.position      = request.position;
   m_request.position_by   = request.position_by;
//-----------------------------------------------------
   bool bool_var = CTP::OrderCheck(request,check_result);
//-----------------------------------------------------
   m_check_result.retcode       = check_result.retcode;
   m_check_result.balance       = check_result.balance;
   m_check_result.equity        = check_result.equity;
   m_check_result.profit        = check_result.profit;
   m_check_result.margin        = check_result.margin;
   m_check_result.margin_free   = check_result.margin_free;
   m_check_result.margin_level  = check_result.margin_level;
   m_check_result.comment       = check_result.comment;
   return(bool_var);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CTrade::OrderSend(MqlTradeRequest &request,MqlTradeResult &result)
  {
   ::ZeroMemory(m_request);
   ::ZeroMemory(m_result);
   m_request.action        = request.action;
   m_request.symbol        = request.symbol;
   m_request.magic         = request.magic;
   m_request.order         = request.order;
   m_request.volume        = request.volume;
   m_request.price         = request.price;
   m_request.stoplimit     = request.stoplimit;
   m_request.sl            = request.sl;
   m_request.tp            = request.tp;
   m_request.deviation     = request.deviation;
   m_request.type          = request.type;
   m_request.type_filling  = request.type_filling;
   m_request.type_time     = request.type_time;
   m_request.expiration    = request.expiration;
   m_request.comment       = request.comment;
   m_request.position      = request.position;
   m_request.position_by   = request.position_by;
//-----------------------------------------------------
   bool bool_var = CTP::OrderSend(request,result);
//-----------------------------------------------------
   m_result.retcode           = result.retcode;
   m_result.deal              = result.deal;
   m_result.order             = result.order;
   m_result.volume            = result.volume;
   m_result.price             = result.price;
   m_result.bid               = result.bid;
   m_result.ask               = result.ask;
   m_result.comment           = result.comment;
   m_result.request_id        = result.request_id;
   m_result.retcode_external  = result.retcode_external;
   return(bool_var);
  }
//+------------------------------------------------------------------+
//| Clear structures m_request,m_result and m_check_result           |
//+------------------------------------------------------------------+
void CTrade::ClearStructures(void)
  {
   ::ZeroMemory(m_request);
   ::ZeroMemory(m_result);
   ::ZeroMemory(m_check_result);
  }
//+------------------------------------------------------------------+
//| Checks forced shutdown of MQL5-program                           |
//+------------------------------------------------------------------+
bool CTrade::IsStopped(const string function)
  {
   if(!::IsStopped())
      return(false);
//--- MQL5 program is stopped
   ::PrintFormat("%s: MQL5 program is stopped. Trading is disabled",function);
   m_result.retcode=TRADE_RETCODE_CLIENT_DISABLES_AT;
   return(true);
  }
//+------------------------------------------------------------------+
//| methods for working with event                                   |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| 事件信息提取                                                     |
//+------------------------------------------------------------------+
string CTrade::ExchangeID(long lparam)
  {
   return(CTP::ExchangeID((CTP::ENUM_EXCHANGE)lparam));
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string CTrade::OrderRef(string sparam)
  {
   return(sparam);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string CTrade::OrderSysID(string sparam)
  {
   return(sparam);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string CTrade::TradeID(double dparam)
  {
   return(::StringFormat("%12d",(long)dparam));
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
long CTrade::FrontID(long lparam)
  {
   return(lparam);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
long CTrade::SessionID(double dparam)
  {
   return((long)dparam);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string CTrade::OrderTicket(const long lparam,const double dparam,const string sparam)
  {
   return(::StringFormat("%s.%d.%d",sparam,lparam,(long)dparam));
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string CTrade::DealTicket(const long lparam,const double dparam,const string sparam)
  {
   return(::StringFormat("%s.%s.%s",CTP::ExchangeID((CTP::ENUM_EXCHANGE)lparam),sparam,TradeID(dparam)));
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CTrade::OnChartEvent(const int id,const long &lparam,const double &dparam,const string &sparam)
  {
   if(id>CHARTEVENT_CUSTOM)
     {
      switch(id)
        {
         // 订单事件  报单/撤单/成交多次响应
         case EXPERT_CTP_ORDER:
            // lparam:FrontID(int)
            // dparam:SessionID(int)
            // sparam:OrderRef(int)
            // Order/HistoryOrder Ticket:string(OrderRef.FrontID.SessionID)
            OnOrder(id,lparam,dparam,sparam);
            break;
         // 成交事件
         case EXPERT_CTP_TRADE:
            // lparam:ExchangeID(int)  交易所编码
            // dparam:TradeID(int)  成交编号/单笔报单的多笔成交
            // sparam:OrderSysID(string)  交易所报单编号/关联历史成交和历史报单
            // HistoryDeal Ticket:string(ExchangeID.OrderSysID.TradeID)   可确认唯一成交
            OnTrade(id,lparam,dparam,sparam);
            break;
         // 错误事件
         case EXPERT_CTP_ERROR:
            // lparam:FrontID(int)
            // dparam:SessionID(int)
            // sparam:OrderRef(int)
            // Order/HistoryOrder Ticket:string(OrderRef.FrontID.SessionID)
            OnError(id,lparam,dparam,sparam);
            break;
         default:
            break;
        }
     }
   return;
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
