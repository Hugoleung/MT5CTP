//+------------------------------------------------------------------+
//|                                             HistoryOrderInfo.mqh |
//+------------------------------------------------------------------+
#property copyright     "Copyright 2020,Guorui Holding (Shandong) Co.,Ltd."
#property version       "1.50"
#property link          ""
#property description   "本程序仅用于量化投资爱好者测试、研究。程序使用者承诺："
#property description   "1.本人/本单位熟知本程序的适用范围，自愿承担因商业应用造成的违法、违规、侵权责任。"
#property description   "2.本人/本单位熟知金融衍生品的各种交易及代理风险，并自愿承担损失及违法、违规、违约责任。"
#property description   "3.本人/本单位熟知计算机程序的各种固有风险，并自愿承担损失及违法、违规、违约责任。"
#property description   "4.本人/本单位熟知金融监管规则，依法依规参与金融市场，并自愿承担损失及违法、违规责任。"
#property description   "5.本人/本单位认可开始使用本程序，视为同意并做出上述承诺，停用本程序后，上述承诺依然有效。"
//+------------------------------------------------------------------+
//| include 外部依赖                                                 |
//+------------------------------------------------------------------+
#include <Object.mqh>
#include "mt5ctp.mqh"
//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
class CHistoryOrderInfo : public CObject
  {
protected:
   CTPHistoryOrders  m_order;             // 选中的历史报单数据
   string            m_ticket;            // 选中的历史报单数据key
public:
                     CHistoryOrderInfo(void);
                    ~CHistoryOrderInfo(void);
   //--- fast access methods to the string deal propertyes
   // 报单编码（mt5ctp系统编码）
   string            Ticket(void) const { return(m_ticket); }
   // 经纪公司代码
   string            BrokerID(void) const;
   // 投资者代码
   string            InvestorID(void) const;
   // 合约代码
   string            Symbol(void) const;
   // 用户代码
   string            UserID(void) const;
   // 组合开平标志
   long              OpenClose(void) const;
   string            Offset(void) const;
   // 组合投机套保标志
   string            Hedge(void) const;
   // GTD日期
   string            GTDDate(void) const;
   // 业务单元
   string            BusinessUnit(void) const;
   // 本地报单编号
   string            OrderLocalID(void) const;
   // 交易所代码
   string            ExchangeID(void) const;
   // 会员代码
   string            ParticipantID(void) const;
   // 客户代码
   string            ClientID(void) const;
   // 合约在交易所的代码
   string            ExchangeInstID(void) const;
   // 交易所交易员代码
   string            TraderID(void) const;
   // 交易日
   string            TradingDay(void) const;
   // 报单编号
   string            OrderSysID(void) const;
   // 报单日期
   string            InsertDate(void) const;
   // 委托时间
   string            InsertTime(void) const;
   // 激活时间
   string            ActiveTime(void) const;
   // 挂起时间
   string            SuspendTime(void) const;
   // 最后修改时间
   string            UpdateTime(void) const;
   // 撤销时间
   string            CancelTime(void) const;
   // 最后修改交易所交易员代码
   string            ActiveTraderID(void) const;
   // 用户端产品信息
   string            UserProductInfo(void) const;
   // 状态信息
   string            StatusMsg(void) const;
   // 操作用户代码
   string            ActiveUserID(void) const;
   // 相关报单
   string            RelativeOrderSysID(void) const;
   // 营业部编号
   string            BranchID(void) const;
   // 投资单元代码
   string            InvestUnitID(void) const;
   // 资金账号
   string            AccountID(void) const;
   // 币种代码
   string            CurrencyID(void) const;
   // IP地址
   string            IPAddress(void) const;
   // Mac地址
   string            MacAddress(void) const;
   // 错误信息
   string            ErrorMsg(void) const;
   //--- fast access methods to the double order propertyes
   // 价格
   double            LimitPrice(void) const;
   // 触发价
   double            StopPrice(void) const;
   // 止损价
   double            StopLoss(void) const;
   // 止赢价
   double            TakeProfit(void) const;
   //--- fast access methods to the integer order propertyes
   //报单总数
   int               ToTal(void) const;
   // 报单时间
   datetime          Time(void) const;
   // 报单引用
   long              OrderRef(void) const;
   // 报单价格条件
   char              OrderPriceType(void) const;
   string            OrderPriceTypeDescription(void) const;
   // 买卖方向
   ENUM_ORDER_TYPE   OrderTypeMt5(void) const;
   char              Direction(void) const;
   string            DirectionDescription(void) const;
   // 有效期类型
   char              TimeCondition(void) const;
   string            TimeConditionDescription(void) const;
   // 报单数量
   long              VolumeTotal(void) const;
   // 成交数量
   long              VolumeTraded(void) const;
   // 剩余数量
   long              Volume(void) const;
   // 最小成交量
   long              MinVolume(void) const;
   // 成交量类型
   char              VolumeCondition(void) const;
   string            VolumeConditionDescription(void) const;
   // 触发条件
   char              ContingentCondition(void) const;
   string            ContingentConditionDescription(void) const;
   // 强平原因
   char              ForceCloseReason(void) const;
   string            ForceCloseReasonDescription(void) const;
   // 是否自动挂起
   bool              IsAutoSuspend(void) const;
   // 请求编号
   long              RequestID(void) const;
   // 安装编号
   long              InstallID(void) const;
   // 报单提交状态
   char              OrderSubmitStatus(void) const;
   string            OrderSubmitStatusDescription(void) const;
   // 报单提示序号
   long              NotifySequence(void) const;
   // 结算编号
   long              SettlementID(void) const;
   // 报单来源
   char              OrderSource(void) const;
   string            OrderSourceDescription(void) const;
   // 报单状态
   char              OrderStatus(void) const;
   string            OrderStatusDescription(void) const;
   // 报单类型
   char              OrderType(void) const;
   string            OrderTypeDescription(void) const;
   // 序号
   long              SequenceNo(void) const;
   // 前置编号
   long              FrontID(void) const;
   // 会话编号
   long              SessionID(void) const;
   // 用户强评标志
   bool              UserForceClose(void) const;
   // 经纪公司报单编号
   long              BrokerOrderSeq(void) const;
   // 郑商所成交数量
   long              ZCETotalTradedVolume(void) const;
   // 互换单标志
   bool              IsSwapOrder(void) const;
   // 错误代码
   long              ErrorID(void) const;
   //--- access methods to the API MQL5 functions
   bool              InfoInteger(const CTP::ENUM_ORDER_PROPERTY_INTEGER prop_id,long &var) const;
   bool              InfoDouble(const CTP::ENUM_ORDER_PROPERTY_DOUBLE prop_id,double &var) const;
   bool              InfoString(const CTP::ENUM_ORDER_PROPERTY_STRING prop_id,string &var) const;
   //--- method for select history order
   bool              Select(const string ticket);
   bool              SelectByIndex(const int index);
  };
//+------------------------------------------------------------------+
//| Constructor                                                      |
//+------------------------------------------------------------------+
CHistoryOrderInfo::CHistoryOrderInfo(void)
  {
   ::ZeroMemory(m_order);
  }
//+------------------------------------------------------------------+
//| Destructor                                                       |
//+------------------------------------------------------------------+
CHistoryOrderInfo::~CHistoryOrderInfo(void)
  {
  }
//+------------------------------------------------------------------+
//| fast access methods to the string history order propertyes       |
//+------------------------------------------------------------------+
// 经纪公司代码
string CHistoryOrderInfo::BrokerID(void) const
  {
   return(::CharArrayToString(m_order.BrokerID));
  }
// 投资者代码
string CHistoryOrderInfo::InvestorID(void) const
  {
   return(::CharArrayToString(m_order.InvestorID));
  }
// 合约代码
string CHistoryOrderInfo::Symbol(void) const
  {
   return(::CharArrayToString(m_order.InstrumentID));
  }
// 用户代码
string CHistoryOrderInfo::UserID(void) const
  {
   return(::CharArrayToString(m_order.UserID));
  }
// 组合开平标志
long CHistoryOrderInfo::OpenClose(void) const
  {
   long open_close = ORDER_TYPE_EXIT;
   if(m_order.CombOffsetFlag[0] == '0')
     {
      open_close = ORDER_TYPE_ENTRY;
     }
   else
      if(m_order.CombOffsetFlag[0] == '3')
        {
         open_close = ORDER_TYPE_EXITTODAY;
        }
   return(open_close);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string CHistoryOrderInfo::Offset(void) const
  {
   return(::CharArrayToString(m_order.CombOffsetFlag));
  }
// 组合投机套保标志
string CHistoryOrderInfo::Hedge(void) const
  {
   return(::CharArrayToString(m_order.CombHedgeFlag));
  }
// GTD日期
string CHistoryOrderInfo::GTDDate(void) const
  {
   return(::CharArrayToString(m_order.GTDDate));
  }
// 业务单元
string CHistoryOrderInfo::BusinessUnit(void) const
  {
   return(::CharArrayToString(m_order.BusinessUnit));
  }
// 本地报单编号
string CHistoryOrderInfo::OrderLocalID(void) const
  {
   return(::CharArrayToString(m_order.OrderLocalID));
  }
// 交易所代码
string CHistoryOrderInfo::ExchangeID(void) const
  {
   return(::CharArrayToString(m_order.ExchangeID));
  }
// 会员代码
string CHistoryOrderInfo::ParticipantID(void) const
  {
   return(::CharArrayToString(m_order.ParticipantID));
  }
// 客户代码
string CHistoryOrderInfo::ClientID(void) const
  {
   return(::CharArrayToString(m_order.ClientID));
  }
// 合约在交易所的代码
string CHistoryOrderInfo::ExchangeInstID(void) const
  {
   return(::CharArrayToString(m_order.ExchangeInstID));
  }
// 交易所交易员代码
string CHistoryOrderInfo::TraderID(void) const
  {
   return(::CharArrayToString(m_order.TraderID));
  }
// 交易日
string CHistoryOrderInfo::TradingDay(void) const
  {
   return(::CharArrayToString(m_order.TradingDay));
  }
// 报单编号
string CHistoryOrderInfo::OrderSysID(void) const
  {
   return(::CharArrayToString(m_order.OrderSysID));
  }
// 报单日期
string CHistoryOrderInfo::InsertDate(void) const
  {
   return(::CharArrayToString(m_order.InsertDate));
  }
// 委托时间
string CHistoryOrderInfo::InsertTime(void) const
  {
   return(::CharArrayToString(m_order.InsertTime));
  }
// 激活时间
string CHistoryOrderInfo::ActiveTime(void) const
  {
   return(::CharArrayToString(m_order.ActiveTime));
  }
// 挂起时间
string CHistoryOrderInfo::SuspendTime(void) const
  {
   return(::CharArrayToString(m_order.SuspendTime));
  }
// 最后修改时间
string CHistoryOrderInfo::UpdateTime(void) const
  {
   return(::CharArrayToString(m_order.UpdateTime));
  }
// 撤销时间
string CHistoryOrderInfo::CancelTime(void) const
  {
   return(::CharArrayToString(m_order.CancelTime));
  }
// 最后修改交易所交易员代码
string CHistoryOrderInfo::ActiveTraderID(void) const
  {
   return(::CharArrayToString(m_order.ActiveTraderID));
  }
// 用户端产品信息
string CHistoryOrderInfo::UserProductInfo(void) const
  {
   return(::CharArrayToString(m_order.UserProductInfo));
  }
// 状态信息
string CHistoryOrderInfo::StatusMsg(void) const
  {
   return(::CharArrayToString(m_order.StatusMsg));
  }
// 操作用户代码
string CHistoryOrderInfo::ActiveUserID(void) const
  {
   return(::CharArrayToString(m_order.ActiveUserID));
  }
// 相关报单
string CHistoryOrderInfo::RelativeOrderSysID(void) const
  {
   return(::CharArrayToString(m_order.RelativeOrderSysID));
  }
// 营业部编号
string CHistoryOrderInfo::BranchID(void) const
  {
   return(::CharArrayToString(m_order.BranchID));
  }
// 投资单元代码
string CHistoryOrderInfo::InvestUnitID(void) const
  {
   return(::CharArrayToString(m_order.InvestUnitID));
  }
// 资金账号
string CHistoryOrderInfo::AccountID(void) const
  {
   return(::CharArrayToString(m_order.AccountID));
  }
// 币种代码
string CHistoryOrderInfo::CurrencyID(void) const
  {
   return(::CharArrayToString(m_order.CurrencyID));
  }
// IP地址
string CHistoryOrderInfo::IPAddress(void) const
  {
   return(::CharArrayToString(m_order.IPAddress));
  }
// Mac地址
string CHistoryOrderInfo::MacAddress(void) const
  {
   return(::CharArrayToString(m_order.MacAddress));
  }
// 错误信息
string CHistoryOrderInfo::ErrorMsg(void) const
  {
   return(::CharArrayToString(m_order.ErrorMsg));
  }
//+------------------------------------------------------------------+
//| fast access methods to the double history order propertyes       |
//+------------------------------------------------------------------+
// 价格
double CHistoryOrderInfo::LimitPrice(void) const
  {
   return(m_order.LimitPrice);
  }
// 触发价
double CHistoryOrderInfo::StopPrice(void) const
  {
   return(m_order.StopPrice);
  }
// 止损价
double CHistoryOrderInfo::StopLoss(void) const
  {
   return(m_order.StopLoss);
  }
// 止赢价
double CHistoryOrderInfo::TakeProfit(void) const
  {
   return(m_order.TakeProfit);
  }
//+------------------------------------------------------------------+
//| fast access methods to the integer history order propertyes      |
//+------------------------------------------------------------------+
//报单总数
int CHistoryOrderInfo::ToTal(void) const
  {
   return(mt5ctp::HistoryOrdersTotal());
  }
// 报单时间
datetime CHistoryOrderInfo::Time(void) const
  {
   string time = ::CharArrayToString(m_order.InsertDate)+" "+::CharArrayToString(m_order.InsertTime);
   return(::StringToTime(time));
  }
// 报单引用
long CHistoryOrderInfo::OrderRef(void) const
  {
   return(::StringToInteger(::CharArrayToString(m_order.OrderRef)));
  }
// 报单价格条件
char CHistoryOrderInfo::OrderPriceType(void) const
  {
   return(m_order.OrderPriceType);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string CHistoryOrderInfo::OrderPriceTypeDescription(void) const
  {
   string str_res;
   switch(m_order.OrderPriceType)
     {
      case '1':
         str_res = "THOST_FTDC_OPT_AnyPrice:任意价";
         break;
      case '2':
         str_res = "THOST_FTDC_OPT_LimitPrice:限价";
         break;
      case '3':
         str_res = "THOST_FTDC_OPT_BestPrice:最优价";
         break;
      case '4':
         str_res = "THOST_FTDC_OPT_LastPrice:最新价";
         break;
      case '5':
         str_res = "THOST_FTDC_OPT_LastPricePlusOneTicks:最新价浮动上浮1个ticks";
         break;
      case '6':
         str_res = "THOST_FTDC_OPT_LastPricePlusTwoTicks:最新价浮动上浮2个ticks";
         break;
      case '7':
         str_res = "THOST_FTDC_OPT_LastPricePlusThreeTicks:最新价浮动上浮3个ticks";
         break;
      case '8':
         str_res = "THOST_FTDC_OPT_AskPrice1:卖一价";
         break;
      case '9':
         str_res = "THOST_FTDC_OPT_AskPrice1PlusOneTicks:卖一价浮动上浮1个ticks";
         break;
      case 'A':
         str_res = "THOST_FTDC_OPT_AskPrice1PlusTwoTicks:卖一价浮动上浮2个ticks";
         break;
      case 'B':
         str_res = "THOST_FTDC_OPT_AskPrice1PlusThreeTicks:卖一价浮动上浮3个ticks";
         break;
      case 'C':
         str_res = "THOST_FTDC_OPT_BidPrice1:买一价";
         break;
      case 'D':
         str_res = "THOST_FTDC_OPT_BidPrice1PlusOneTicks:买一价浮动上浮1个ticks";
         break;
      case 'E':
         str_res = "THOST_FTDC_OPT_BidPrice1PlusTwoTicks:买一价浮动上浮2个ticks";
         break;
      case 'F':
         str_res = "THOST_FTDC_OPT_BidPrice1PlusThreeTicks:买一价浮动上浮3个ticks";
         break;
      case 'G':
         str_res = "THOST_FTDC_OPT_FiveLevelPrice:五档价";
         break;
      default:
         str_res = "Unknown OrderPriceType";
         break;
     }
   return(str_res);
  }
// 买卖方向
ENUM_ORDER_TYPE CHistoryOrderInfo::OrderTypeMt5(void) const
  {
   ENUM_ORDER_TYPE order_type = ORDER_TYPE_BUY;
   if(m_order.Direction == CTP::ORDER_TYPE_SELL)
     {
      order_type = ORDER_TYPE_SELL;
     }
   return(order_type);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
char CHistoryOrderInfo::Direction(void) const
  {
   return(m_order.Direction);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string CHistoryOrderInfo::DirectionDescription(void) const
  {
   string str_res;
   switch(m_order.Direction)
     {
      case '0':
         str_res = "THOST_FTDC_D_Buy:买";
         break;
      case '1':
         str_res = "THOST_FTDC_D_Sell:卖";
         break;
      default:
         str_res = "Unknown OrderPriceType";
         break;
     }
   return(str_res);
  }
// 有效期类型
char CHistoryOrderInfo::TimeCondition(void) const
  {
   return(m_order.TimeCondition);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string CHistoryOrderInfo::TimeConditionDescription(void) const
  {
   string str_res;
   switch(m_order.TimeCondition)
     {
      case '1':
         str_res = "THOST_FTDC_TC_IOC:立即完成，否则撤销";
         break;
      case '2':
         str_res = "THOST_FTDC_TC_GFS:本节有效";
         break;
      case '3':
         str_res = "THOST_FTDC_TC_GFD:当日有效";
         break;
      case '4':
         str_res = "THOST_FTDC_TC_GTD:指定日期前有效";
         break;
      case '5':
         str_res = "THOST_FTDC_TC_GTC:撤销前有效";
         break;
      case '6':
         str_res = "THOST_FTDC_TC_GFA:集合竞价有效";
         break;
      default:
         str_res = "Unknown TimeCondition";
         break;
     }
   return(str_res);
  }
// 报单数量
long CHistoryOrderInfo::VolumeTotal(void) const
  {
   return(m_order.VolumeTotalOriginal);
  }
// 成交数量
long CHistoryOrderInfo::VolumeTraded(void) const
  {
   return(m_order.VolumeTraded);
  }
// 剩余数量
long CHistoryOrderInfo::Volume(void) const
  {
   return(m_order.VolumeTotal);
  }
// 最小成交量
long CHistoryOrderInfo::MinVolume(void) const
  {
   return(m_order.MinVolume);
  }
// 成交量类型
char CHistoryOrderInfo::VolumeCondition(void) const
  {
   return(m_order.VolumeCondition);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string CHistoryOrderInfo::VolumeConditionDescription(void) const
  {
   string str_res;
   switch(m_order.VolumeCondition)
     {
      case '1':
         str_res = "THOST_FTDC_VC_AV:任何数量";
         break;
      case '2':
         str_res = "THOST_FTDC_VC_MV:最小数量";
         break;
      case '3':
         str_res = "THOST_FTDC_VC_CV:全部数量";
         break;
      default:
         str_res = "Unknown VolumeCondition";
         break;
     }
   return(str_res);
  }
// 触发条件
char CHistoryOrderInfo::ContingentCondition(void) const
  {
   return(m_order.ContingentCondition);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string CHistoryOrderInfo::ContingentConditionDescription(void) const
  {
   string str_res;
   switch(m_order.ContingentCondition)
     {
      case '1':
         str_res = "THOST_FTDC_CC_Immediately:立即";
         break;
      case '2':
         str_res = "THOST_FTDC_CC_Touch:止损";
         break;
      case '3':
         str_res = "THOST_FTDC_CC_TouchProfit:止赢";
         break;
      case '4':
         str_res = "THOST_FTDC_CC_ParkedOrder:预埋单";
         break;
      case '5':
         str_res = "THOST_FTDC_CC_LastPriceGreaterThanStopPrice:最新价大于条件价";
         break;
      case '6':
         str_res = "THOST_FTDC_CC_LastPriceGreaterEqualStopPrice:最新价大于等于条件价";
         break;
      case '7':
         str_res = "THOST_FTDC_CC_LastPriceLesserThanStopPrice:最新价小于条件价";
         break;
      case '8':
         str_res = "THOST_FTDC_CC_LastPriceLesserEqualStopPrice:最新价小于等于条件价";
         break;
      case '9':
         str_res = "THOST_FTDC_CC_AskPriceGreaterThanStopPrice:卖一价大于条件价";
         break;
      case 'A':
         str_res = "THOST_FTDC_CC_AskPriceGreaterEqualStopPrice:卖一价大于等于条件价";
         break;
      case 'B':
         str_res = "THOST_FTDC_CC_AskPriceLesserThanStopPrice:卖一价小于条件价";
         break;
      case 'C':
         str_res = "THOST_FTDC_CC_AskPriceLesserEqualStopPrice:卖一价小于等于条件价";
         break;
      case 'D':
         str_res = "THOST_FTDC_CC_BidPriceGreaterThanStopPrice:买一价大于条件价";
         break;
      case 'E':
         str_res = "THOST_FTDC_CC_BidPriceGreaterEqualStopPrice:买一价大于等于条件价";
         break;
      case 'F':
         str_res = "THOST_FTDC_CC_BidPriceLesserThanStopPrice:买一价小于条件价";
         break;
      case 'H':
         str_res = "THOST_FTDC_CC_BidPriceLesserEqualStopPrice:买一价小于等于条件价";
         break;
      default:
         str_res = "Unknown ContingentCondition";
         break;
     }
   return(str_res);
  }
// 强平原因
char CHistoryOrderInfo::ForceCloseReason(void) const
  {
   return(m_order.ForceCloseReason);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string CHistoryOrderInfo::ForceCloseReasonDescription(void) const
  {
   string str_res;
   switch(m_order.ForceCloseReason)
     {
      case '0':
         str_res = "THOST_FTDC_FCC_NotForceClose:非强平";
         break;
      case '1':
         str_res = "THOST_FTDC_FCC_LackDeposit:资金不足";
         break;
      case '2':
         str_res = "THOST_FTDC_FCC_ClientOverPositionLimit:客户超仓";
         break;
      case '3':
         str_res = "THOST_FTDC_FCC_MemberOverPositionLimit:会员超仓";
         break;
      case '4':
         str_res = "THOST_FTDC_FCC_NotMultiple:持仓非整数倍";
         break;
      case '5':
         str_res = "THOST_FTDC_FCC_Violation:违规";
         break;
      case '6':
         str_res = "THOST_FTDC_FCC_Other:其它";
         break;
      case '7':
         str_res = "THOST_FTDC_FCC_PersonDeliv:自然人临近交割";
         break;
      default:
         str_res = "Unknown ForceCloseReason";
         break;
     }
   return(str_res);
  }
// 是否自动挂起
bool CHistoryOrderInfo::IsAutoSuspend(void) const
  {
   return(m_order.IsAutoSuspend!=0);
  }
// 请求编号
long CHistoryOrderInfo::RequestID(void) const
  {
   return(m_order.RequestID);
  }
// 安装编号
long CHistoryOrderInfo::InstallID(void) const
  {
   return(m_order.InstallID);
  }
// 报单提交状态
char CHistoryOrderInfo::OrderSubmitStatus(void) const
  {
   return(m_order.OrderSubmitStatus);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string CHistoryOrderInfo::OrderSubmitStatusDescription(void) const
  {
   string str_res;
   switch(m_order.OrderSubmitStatus)
     {
      case '0':
         str_res = "THOST_FTDC_OSS_InsertSubmitted:已经提交";
         break;
      case '1':
         str_res = "THOST_FTDC_OSS_CancelSubmitted:撤单已经提交";
         break;
      case '2':
         str_res = "THOST_FTDC_OSS_ModifySubmitted:修改已经提交";
         break;
      case '3':
         str_res = "THOST_FTDC_OSS_Accepted:已经接受";
         break;
      case '4':
         str_res = "THOST_FTDC_OSS_InsertRejected:报单已经被拒绝";
         break;
      case '5':
         str_res = "THOST_FTDC_OSS_CancelRejected:撤单已经被拒绝";
         break;
      case '6':
         str_res = "THOST_FTDC_OSS_ModifyRejected:改单已经被拒绝";
         break;
      default:
         str_res = "Unknown OrderSubmitStatus";
         break;
     }
   return(str_res);
  }
// 报单提示序号
long CHistoryOrderInfo::NotifySequence(void) const
  {
   return(m_order.NotifySequence);
  }
// 结算编号
long CHistoryOrderInfo::SettlementID(void) const
  {
   return(m_order.SettlementID);
  }
// 报单来源
char CHistoryOrderInfo::OrderSource(void) const
  {
   return(m_order.OrderSource);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string CHistoryOrderInfo::OrderSourceDescription(void) const
  {
   string str_res;
   switch(m_order.OrderSource)
     {
      case '0':
         str_res = "THOST_FTDC_OSRC_Participant:来自参与者";
         break;
      case '1':
         str_res = "THOST_FTDC_OSRC_Administrator:来自管理员";
         break;
      default:
         str_res = "Unknown OrderSubmitStatus";
         break;
     }
   return(str_res);
  }
// 报单状态
char CHistoryOrderInfo::OrderStatus(void) const
  {
   return(m_order.OrderStatus);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string CHistoryOrderInfo::OrderStatusDescription(void) const
  {
   string str_res;
   switch(m_order.OrderStatus)
     {
      case '0':
         str_res = "THOST_FTDC_OST_AllTraded:全部成交";
         break;
      case '1':
         str_res = "THOST_FTDC_OST_PartTradedQueueing:部分成交还在队列中";
         break;
      case '2':
         str_res = "THOST_FTDC_OST_PartTradedNotQueueing:部分成交不在队列中";
         break;
      case '3':
         str_res = "THOST_FTDC_OST_NoTradeQueueing:未成交还在队列中";
         break;
      case '4':
         str_res = "THOST_FTDC_OST_NoTradeNotQueueing:未成交不在队列中";
         break;
      case '5':
         str_res = "THOST_FTDC_OST_Canceled:撤单";
         break;
      case 'a':
         str_res = "THOST_FTDC_OST_Unknown:未知";
         break;
      case 'b':
         str_res = "THOST_FTDC_OST_NotTouched:尚未触发";
         break;
      case 'c':
         str_res = "THOST_FTDC_OST_Touched:已触发";
         break;
      default:
         str_res = "Unknown OrderSource";
         break;
     }
   return(str_res);
  }
// 报单类型
char CHistoryOrderInfo::OrderType(void) const
  {
   return(m_order.OrderType);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
string CHistoryOrderInfo::OrderTypeDescription(void) const
  {
   string str_res;
   switch(m_order.OrderType)
     {
      case '0':
         str_res = "THOST_FTDC_ORDT_Normal:正常";
         break;
      case '1':
         str_res = "THOST_FTDC_ORDT_DeriveFromQuote:报价衍生";
         break;
      case '2':
         str_res = "THOST_FTDC_ORDT_DeriveFromCombination:组合衍生";
         break;
      case '3':
         str_res = "THOST_FTDC_ORDT_Combination:组合报单";
         break;
      case '4':
         str_res = "THOST_FTDC_ORDT_ConditionalOrder:条件单";
         break;
      case '5':
         str_res = "THOST_FTDC_ORDT_Swap:互换单";
         break;
      case '6':
         str_res = "THOST_FTDC_ORDT_DeriveFromBlockTrade:大宗交易成交衍生";
         break;
      case '7':
         str_res = "THOST_FTDC_ORDT_DeriveFromEFPTrade:期转现成交衍生";
         break;
      default:
         str_res = "Unknown OrderType";
         break;
     }
   return(str_res);
  }
// 序号
long CHistoryOrderInfo::SequenceNo(void) const
  {
   return(m_order.SequenceNo);
  }
// 前置编号
long CHistoryOrderInfo::FrontID(void) const
  {
   return(m_order.FrontID);
  }
// 会话编号
long CHistoryOrderInfo::SessionID(void) const
  {
   return(m_order.SessionID);
  }
// 用户强评标志
bool CHistoryOrderInfo::UserForceClose(void) const
  {
   return(m_order.UserForceClose!=0);
  }
// 经纪公司报单编号
long CHistoryOrderInfo::BrokerOrderSeq(void) const
  {
   return(m_order.BrokerOrderSeq);
  }
// 郑商所成交数量
long CHistoryOrderInfo::ZCETotalTradedVolume(void) const
  {
   return(m_order.ZCETotalTradedVolume);
  }
// 互换单标志
bool CHistoryOrderInfo::IsSwapOrder(void) const
  {
   return(m_order.IsSwapOrder!=0);
  }
// 错误代码
long CHistoryOrderInfo::ErrorID(void) const
  {
   return(m_order.ErrorID);
  }
//+------------------------------------------------------------------+
//| Access functions HistoryOrderInfoInteger(...)                    |
//+------------------------------------------------------------------+
bool CHistoryOrderInfo::InfoInteger(const CTP::ENUM_ORDER_PROPERTY_INTEGER prop_id,long &var) const
  {
   bool bool_res = false;
   switch(prop_id)
     {
      case CTP::ORDER_OrderPriceType:
         var = (long)m_order.OrderPriceType;
         bool_res = true;
         break;
      case CTP::ORDER_Direction:
         var = (long)m_order.Direction;
         bool_res = true;
         break;
      case CTP::ORDER_VolumeTotalOriginal:
         var = (long)m_order.VolumeTotalOriginal;
         bool_res = true;
         break;
      case CTP::ORDER_TimeCondition:
         var = (long)m_order.TimeCondition;
         bool_res = true;
         break;
      case CTP::ORDER_VolumeCondition:
         var = (long)m_order.VolumeCondition;
         bool_res = true;
         break;
      case CTP::ORDER_MinVolume:
         var = (long)m_order.MinVolume;
         bool_res = true;
         break;
      case CTP::ORDER_ContingentCondition:
         var = (long)m_order.ContingentCondition;
         bool_res = true;
         break;
      case CTP::ORDER_ForceCloseReason:
         var = (long)m_order.ForceCloseReason;
         bool_res = true;
         break;
      case CTP::ORDER_IsAutoSuspend:
         var = (long)m_order.IsAutoSuspend;
         bool_res = true;
         break;
      case CTP::ORDER_RequestID:
         var = (long)m_order.RequestID;
         bool_res = true;
         break;
      case CTP::ORDER_InstallID:
         var = (long)m_order.InstallID;
         bool_res = true;
         break;
      case CTP::ORDER_OrderSubmitStatus:
         var = (long)m_order.OrderSubmitStatus;
         bool_res = true;
         break;
      case CTP::ORDER_NotifySequence:
         var = (long)m_order.NotifySequence;
         bool_res = true;
         break;
      case CTP::ORDER_SettlementID:
         var = (long)m_order.SettlementID;
         bool_res = true;
         break;
      case CTP::ORDER_OrderSource:
         var = (long)m_order.OrderSource;
         bool_res = true;
         break;
      case CTP::ORDER_OrderStatus:
         var = (long)m_order.OrderStatus;
         bool_res = true;
         break;
      case CTP::ORDER_OrderType:
         var = (long)m_order.OrderType;
         bool_res = true;
         break;
      case CTP::ORDER_VolumeTraded:
         var = (long)m_order.VolumeTraded;
         bool_res = true;
         break;
      case CTP::ORDER_VolumeTotal:
         var = (long)m_order.VolumeTotal;
         bool_res = true;
         break;
      case CTP::ORDER_SequenceNo:
         var = (long)m_order.SequenceNo;
         bool_res = true;
         break;
      case CTP::ORDER_FrontID:
         var = (long)m_order.FrontID;
         bool_res = true;
         break;
      case CTP::ORDER_SessionID:
         var = (long)m_order.SessionID;
         bool_res = true;
         break;
      case CTP::ORDER_UserForceClose:
         var = (long)m_order.UserForceClose;
         bool_res = true;
         break;
      case CTP::ORDER_BrokerOrderSeq:
         var = (long)m_order.BrokerOrderSeq;
         bool_res = true;
         break;
      case CTP::ORDER_ZCETotalTradedVolume:
         var = (long)m_order.ZCETotalTradedVolume;
         bool_res = true;
         break;
      case CTP::ORDER_IsSwapOrder:
         var = (long)m_order.IsSwapOrder;
         bool_res = true;
         break;
      case CTP::ORDER_ErrorID:
         var = (long)m_order.ErrorID;
         bool_res = true;
         break;
      default:
         break;
     }
   return(bool_res);
  }
//+------------------------------------------------------------------+
//| Access functions HistoryOrderInfoDouble(...)                     |
//+------------------------------------------------------------------+
bool CHistoryOrderInfo::InfoDouble(const CTP::ENUM_ORDER_PROPERTY_DOUBLE prop_id,double &var) const
  {
   bool bool_res = false;
   switch(prop_id)
     {
      case CTP::ORDER_LimitPrice:
         var = m_order.LimitPrice;
         bool_res = true;
         break;
      case CTP::ORDER_StopPrice:
         var = m_order.StopPrice;
         bool_res = true;
         break;
      case CTP::ORDER_StopLoss:
         var = m_order.StopLoss;
         bool_res = true;
         break;
      case CTP::ORDER_TakeProfit:
         var = m_order.TakeProfit;
         bool_res = true;
         break;
      default:
         break;
     }
   return(bool_res);
  }
//+------------------------------------------------------------------+
//| Access functions HistoryOrderInfoString(...)                     |
//+------------------------------------------------------------------+
bool CHistoryOrderInfo::InfoString(const CTP::ENUM_ORDER_PROPERTY_STRING prop_id,string &var) const
  {
   bool bool_res = false;
   switch(prop_id)
     {
      case CTP::ORDER_BrokerID:
         var = ::CharArrayToString(m_order.BrokerID);
         bool_res = true;
         break;
      case CTP::ORDER_InvestorID:
         var = ::CharArrayToString(m_order.InvestorID);
         bool_res = true;
         break;
      case CTP::ORDER_InstrumentID:
         var = ::CharArrayToString(m_order.InstrumentID);
         bool_res = true;
         break;
      case CTP::ORDER_OrderRef:
         var = ::CharArrayToString(m_order.OrderRef);
         bool_res = true;
         break;
      case CTP::ORDER_UserID:
         var = ::CharArrayToString(m_order.UserID);
         bool_res = true;
         break;
      case CTP::ORDER_CombOffsetFlag:
         var = ::CharArrayToString(m_order.CombOffsetFlag);
         bool_res = true;
         break;
      case CTP::ORDER_CombHedgeFlag:
         var = ::CharArrayToString(m_order.CombHedgeFlag);
         bool_res = true;
         break;
      case CTP::ORDER_GTDDate:
         var = ::CharArrayToString(m_order.GTDDate);
         bool_res = true;
         break;
      case CTP::ORDER_BusinessUnit:
         var = ::CharArrayToString(m_order.BusinessUnit);
         bool_res = true;
         break;
      case CTP::ORDER_OrderLocalID:
         var = ::CharArrayToString(m_order.OrderLocalID);
         bool_res = true;
         break;
      case CTP::ORDER_ExchangeID:
         var = ::CharArrayToString(m_order.ExchangeID);
         bool_res = true;
         break;
      case CTP::ORDER_ParticipantID:
         var = ::CharArrayToString(m_order.ParticipantID);
         bool_res = true;
         break;
      case CTP::ORDER_ClientID:
         var = ::CharArrayToString(m_order.ClientID);
         bool_res = true;
         break;
      case CTP::ORDER_ExchangeInstID:
         var = ::CharArrayToString(m_order.ExchangeInstID);
         bool_res = true;
         break;
      case CTP::ORDER_TraderID:
         var = ::CharArrayToString(m_order.TraderID);
         bool_res = true;
         break;
      case CTP::ORDER_TradingDay:
         var = ::CharArrayToString(m_order.TradingDay);
         bool_res = true;
         break;
      case CTP::ORDER_OrderSysID:
         var = ::CharArrayToString(m_order.OrderSysID);
         bool_res = true;
         break;
      case CTP::ORDER_InsertDate:
         var = ::CharArrayToString(m_order.InsertDate);
         bool_res = true;
         break;
      case CTP::ORDER_InsertTime:
         var = ::CharArrayToString(m_order.InsertTime);
         bool_res = true;
         break;
      case CTP::ORDER_ActiveTime:
         var = ::CharArrayToString(m_order.ActiveTime);
         bool_res = true;
         break;
      case CTP::ORDER_SuspendTime:
         var = ::CharArrayToString(m_order.SuspendTime);
         bool_res = true;
         break;
      case CTP::ORDER_UpdateTime:
         var = ::CharArrayToString(m_order.UpdateTime);
         bool_res = true;
         break;
      case CTP::ORDER_CancelTime:
         var = ::CharArrayToString(m_order.CancelTime);
         bool_res = true;
         break;
      case CTP::ORDER_ActiveTraderID:
         var = ::CharArrayToString(m_order.ActiveTraderID);
         bool_res = true;
         break;
      case CTP::ORDER_ClearingPartID:
         var = ::CharArrayToString(m_order.ClearingPartID);
         bool_res = true;
         break;
      case CTP::ORDER_UserProductInfo:
         var = ::CharArrayToString(m_order.UserProductInfo);
         bool_res = true;
         break;
      case CTP::ORDER_StatusMsg:
         var = ::CharArrayToString(m_order.StatusMsg);
         bool_res = true;
         break;
      case CTP::ORDER_ActiveUserID:
         var = ::CharArrayToString(m_order.ActiveUserID);
         bool_res = true;
         break;
      case CTP::ORDER_RelativeOrderSysID:
         var = ::CharArrayToString(m_order.RelativeOrderSysID);
         bool_res = true;
         break;
      case CTP::ORDER_BranchID:
         var = ::CharArrayToString(m_order.BranchID);
         bool_res = true;
         break;
      case CTP::ORDER_InvestUnitID:
         var = ::CharArrayToString(m_order.InvestUnitID);
         bool_res = true;
         break;
      case CTP::ORDER_AccountID:
         var = ::CharArrayToString(m_order.AccountID);
         bool_res = true;
         break;
      case CTP::ORDER_CurrencyID:
         var = ::CharArrayToString(m_order.CurrencyID);
         bool_res = true;
         break;
      case CTP::ORDER_IPAddress:
         var = ::CharArrayToString(m_order.IPAddress);
         bool_res = true;
         break;
      case CTP::ORDER_MacAddress:
         var = ::CharArrayToString(m_order.MacAddress);
         bool_res = true;
         break;
      case CTP::ORDER_ErrorMsg:
         var = ::CharArrayToString(m_order.ErrorMsg);
         bool_res = true;
         break;
      default:
         break;
     }
   return(bool_res);
  }
//+------------------------------------------------------------------+
//| method for select history order                                  |
//+------------------------------------------------------------------+
bool CHistoryOrderInfo::Select(const string ticket)
  {
   ::ZeroMemory(m_order);
   if(mt5ctp::HistoryOrderSelectByTicket(ticket,m_order))
     {
      m_ticket = ticket;
      return(true);
     }
   return(false);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CHistoryOrderInfo::SelectByIndex(const int index)
  {
   string ticket = CTP::HistoryOrderGetTicket(index);
   return(Select(ticket));
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
