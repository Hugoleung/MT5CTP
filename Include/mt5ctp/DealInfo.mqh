//+------------------------------------------------------------------+
//|                                                     DealInfo.mqh |
//+------------------------------------------------------------------+
#property copyright     "Copyright 2020,Guorui Holding (Shandong) Co.,Ltd."
#property version       "1.50"
#property link          ""
#property description   "本程序仅用于量化投资爱好者测试、研究。程序使用者承诺："
#property description   "1.本人/本单位熟知本程序的适用范围，自愿承担因商业应用造成的违法、违规、侵权责任。"
#property description   "2.本人/本单位熟知金融衍生品的各种交易及代理风险，并自愿承担损失及违法、违规、违约责任。"
#property description   "3.本人/本单位熟知计算机程序的各种固有风险，并自愿承担损失及违法、违规、违约责任。"
#property description   "4.本人/本单位熟知金融监管规则，依法依规参与金融市场，并自愿承担损失及违法、违规责任。"
#property description   "5.本人/本单位认可开始使用本程序，视为同意并做出上述承诺，停用本程序后，上述承诺依然有效。"
//+------------------------------------------------------------------+
//| include 外部依赖                                                 |
//+------------------------------------------------------------------+
#include <Object.mqh>
#include "mt5ctp.mqh"
//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
class CDealInfo : public CObject
  {
protected:
   CTPHistoryDeals   m_deal;             // 选中的历史成交数据
   string            m_ticket;           // 选中的历史成交数据key
public:
                     CDealInfo(void);
                    ~CDealInfo(void);
   //--- fast access methods to the string deal propertyes
   // 成交单编码（mt5ctp系统编码）
   string            Ticket(void) const { return(m_ticket); }
   // 报单编码（mt5ctp系统编码）
   string            Order(void) const;
   // 合约代码
   string            Symbol(void) const;
   // 合约在交易所的代码
   string            ExchangeInstID(void) const;
   // 交易所代码
   string            ExchangeID(void) const;
   // 投资者代码
   string            InvestorID(void) const;
   // 用户代码
   string            UserID(void) const;
   // 经纪公司代码
   string            BrokerID(void) const;
   // 成交编号
   string            TradeID(void) const;
   // 报单编号
   string            OrderSysID(void) const;
   // 本地报单编号
   string            OrderLocalID(void) const;
   // 成交日期
   string            TradeDate(void) const;
   // 成交时间
   string            TradeTime(void) const;
   // 交易日
   string            TradingDay(void) const;
   // 业务单元
   string            BusinessUnit(void) const;
   // 投资单元代码
   string            InvestUnitID(void) const;
   //--- fast access methods to the double deal propertyes
   // 价格
   double            Price(void) const;
   //--- fast access methods to the integer deal propertyes
   //成交单总数
   int               ToTal(void) const;
   // 成交时间
   datetime          Time(void) const;
   // 买卖方向
   char              Direction(void) const;
   string            DirectionDescription(void) const;
   // 开平仓
   char              Offset(void) const;
   string            OffsetDescription(void) const;
   // 投机套保标志
   char              Hedge(void) const;
   string            HedgeDescription(void) const;
   // 成交类型
   char              TradeType(void) const;
   string            TradeTypeDescription(void) const;
   // 交易角色
   char              TradingRole(void) const;
   string            TradingRoleDescription(void) const;
   // 成交价格来源
   char              PriceSource(void) const;
   string            PriceSourceDescription(void) const;
   // 成交来源
   char              TradeSource(void) const;
   string            TradeSourceDescription(void) const;
   // 数量
   long              Volume(void) const;
   // 报单引用
   long              OrderRef(void) const;
   // 序号
   long              SequenceNo(void) const;
   // 结算编号
   long              SettlementID(void) const;
   // 经纪公司报单序号
   long              BrokerOrderSeq(void) const;
   //--- access methods to the API MQL5 functions
   bool              InfoInteger(CTP::ENUM_DEAL_PROPERTY_INTEGER prop_id,long &var) const;
   bool              InfoDouble(CTP::ENUM_DEAL_PROPERTY_DOUBLE prop_id,double &var) const;
   bool              InfoString(CTP::ENUM_DEAL_PROPERTY_STRING prop_id,string &var) const;
   //--- method for select deal
   bool              Select(const string ticket);
   bool              SelectByIndex(const int index);
  };
//+------------------------------------------------------------------+
//| Constructor                                                      |
//+------------------------------------------------------------------+
CDealInfo::CDealInfo(void)
  {
   ::ZeroMemory(m_deal);
  }
//+------------------------------------------------------------------+
//| Destructor                                                       |
//+------------------------------------------------------------------+
CDealInfo::~CDealInfo(void)
  {
  }
//+------------------------------------------------------------------+
//| fast access methods to the string deal propertyes                |
//+------------------------------------------------------------------+
// 报单编码（mt5ctp系统编码）
string CDealInfo::Order(void) const
  {
   string str_res;
   int order_index = CTP::HistoryOrdersTotal();
   for(; order_index>0; order_index--)
     {
      if(!CTP::HistoryOrderSelect(order_index))
         continue;
      if(CTP::HistoryOrderGetString(CTP::ORDER_ExchangeID)==ExchangeID() && CTP::HistoryOrderGetString(CTP::ORDER_OrderSysID)==OrderSysID())
        {
         break;
        }
     }
   str_res = CTP::HistoryOrderGetTicket(order_index);
   return(str_res);
  }
// 合约代码
string CDealInfo::Symbol(void) const
  {
   return(::CharArrayToString(m_deal.InstrumentID));
  }
// 合约在交易所的代码
string CDealInfo::ExchangeInstID(void) const
  {
   return(::CharArrayToString(m_deal.ExchangeInstID));
  }
// 交易所代码
string CDealInfo::ExchangeID(void) const
  {
   return(::CharArrayToString(m_deal.ExchangeID));
  }
// 投资者代码
string CDealInfo::InvestorID(void) const
  {
   return(::CharArrayToString(m_deal.InvestorID));
  }
// 用户代码
string CDealInfo::UserID(void) const
  {
   return(::CharArrayToString(m_deal.UserID));
  }
// 经纪公司代码
string CDealInfo::BrokerID(void) const
  {
   return(::CharArrayToString(m_deal.BrokerID));
  }
// 成交编号
string CDealInfo::TradeID(void) const
  {
   return(::CharArrayToString(m_deal.TradeID));
  }
// 报单编号
string CDealInfo::OrderSysID(void) const
  {
   return(::CharArrayToString(m_deal.OrderSysID));
  }
// 本地报单编号
string CDealInfo::OrderLocalID(void) const
  {
   return(::CharArrayToString(m_deal.OrderLocalID));
  }
// 报单引用
long CDealInfo::OrderRef(void) const
  {
   return(::StringToInteger(::CharArrayToString(m_deal.OrderRef)));
  }
// 成交日期
string CDealInfo::TradeDate(void) const
  {
   return(::CharArrayToString(m_deal.TradeDate));
  }
// 成交时间
string CDealInfo::TradeTime(void) const
  {
   return(::CharArrayToString(m_deal.TradeTime));
  }
// 交易日
string CDealInfo::TradingDay(void) const
  {
   return(::CharArrayToString(m_deal.TradingDay));
  }
// 业务单元
string CDealInfo::BusinessUnit(void) const
  {
   return(::CharArrayToString(m_deal.BusinessUnit));
  }
// 投资单元代码
string CDealInfo::InvestUnitID(void) const
  {
   return(::CharArrayToString(m_deal.InvestUnitID));
  }
//+------------------------------------------------------------------+
//| fast access methods to the double deal propertyes                |
//+------------------------------------------------------------------+
// 价格
double CDealInfo::Price(void) const
  {
   return(m_deal.Price);
  }
//+------------------------------------------------------------------+
//| fast access methods to the integer deal propertyes               |
//+------------------------------------------------------------------+
//成交单总数
int CDealInfo::ToTal(void) const
  {
   return(mt5ctp::HistoryDealsTotal());
  }
// 成交时间
datetime CDealInfo::Time(void) const
  {
   string time = ::CharArrayToString(m_deal.TradeDate) + " " + ::CharArrayToString(m_deal.TradeTime);
   return(::StringToTime(time));
  }
// 买卖方向
char CDealInfo::Direction(void) const
  {
   return(m_deal.Direction);
  }
// 买卖方向注释
string CDealInfo::DirectionDescription(void) const
  {
   string str_res;
   switch(m_deal.Direction)
     {
      case '0':
         str_res = "THOST_FTDC_D_Buy:买";
         break;
      case '1':
         str_res = "THOST_FTDC_D_Sell:卖";
         break;
      default:
         str_res = "Unknown Direction";
         break;
     }
   return(str_res);
  }
// 开平仓
char CDealInfo::Offset(void) const
  {
   return(m_deal.OffsetFlag);
  }
// 开平仓注释
string CDealInfo::OffsetDescription(void) const
  {
   string str_res;
   switch(m_deal.OffsetFlag)
     {
      case '0':
         str_res = "THOST_FTDC_OF_Open:开仓";
         break;
      case '1':
         str_res = "THOST_FTDC_OF_Close:平仓";
         break;
      case '2':
         str_res = "THOST_FTDC_OF_ForceClose:强平";
         break;
      case '3':
         str_res = "THOST_FTDC_OF_CloseToday:平今";
         break;
      case '4':
         str_res = "THOST_FTDC_OF_CloseYesterday:平昨";
         break;
      case '5':
         str_res = "THOST_FTDC_OF_ForceOff:强减";
         break;
      case '6':
         str_res = "THOST_FTDC_OF_LocalForceClose:本地强平";
         break;
      default:
         str_res = "Unknown OffsetFlag";
         break;
     }
   return(str_res);
  }
// 投机套保标志
char CDealInfo::Hedge(void) const
  {
   return(m_deal.HedgeFlag);
  }
// 投机套保标志注释
string CDealInfo::HedgeDescription(void) const
  {
   string str_res;
   switch(m_deal.HedgeFlag)
     {
      case '1':
         str_res = "THOST_FTDC_HF_Speculation:投机";
         break;
      case '2':
         str_res = "THOST_FTDC_HF_Arbitrage:套利";
         break;
      case '3':
         str_res = "THOST_FTDC_HF_Hedge:套保";
         break;
      case '5':
         str_res = "THOST_FTDC_HF_MarketMaker:做市商";
         break;
      case '6':
         str_res = "THOST_FTDC_HF_SpecHedge:第一腿投机第二腿套保(大商所专用)";
         break;
      case '7':
         str_res = "THOST_FTDC_HF_HedgeSpec:第一腿套保第二腿投机(大商所专用)";
         break;
      default:
         str_res = "Unknown HedgeFlag";
         break;
     }
   return(str_res);
  }
// 成交类型
char CDealInfo::TradeType(void) const
  {
   return(m_deal.TradeType);
  }
// 成交类型注释
string CDealInfo::TradeTypeDescription(void) const
  {
   string str_res;
   switch(m_deal.TradeType)
     {
      case '#':
         str_res = "THOST_FTDC_TRDT_SplitCombination:组合持仓拆分为单一持仓";
         break;
      case '0':
         str_res = "THOST_FTDC_TRDT_Common:普通成交";
         break;
      case '1':
         str_res = "THOST_FTDC_TRDT_OptionsExecution:期权执行";
         break;
      case '2':
         str_res = "THOST_FTDC_TRDT_OTC:OTC成交";
         break;
      case '3':
         str_res = "THOST_FTDC_TRDT_EFPDerived:期转现衍生成交";
         break;
      case '4':
         str_res = "THOST_FTDC_TRDT_CombinationDerived:组合衍生成交";
         break;
      case '5':
         str_res = "THOST_FTDC_TRDT_BlockTrade:大宗交易成交";
         break;
      default:
         str_res = "Unknown TradeType";
         break;
     }
   return(str_res);
  }
// 交易角色
char CDealInfo::TradingRole(void) const
  {
   return(m_deal.TradingRole);
  }
// 交易角色注释
string CDealInfo::TradingRoleDescription(void) const
  {
   string str_res;
   switch(m_deal.TradingRole)
     {
      case '1':
         str_res = "THOST_FTDC_ER_Broker:代理";
         break;
      case '2':
         str_res = "THOST_FTDC_ER_Host:自营";
         break;
      case '3':
         str_res = "THOST_FTDC_ER_Maker:做市商";
         break;
      default:
         str_res = "Unknown TradingRole";
         break;
     }
   return(str_res);
  }
// 成交价格来源
char CDealInfo::PriceSource(void) const
  {
   return(m_deal.PriceSource);
  }
// 成交价格来源注释
string CDealInfo::PriceSourceDescription(void) const
  {
   string str_res;
   switch(m_deal.PriceSource)
     {
      case '0':
         str_res = "THOST_FTDC_PSRC_LastPrice:前成交价";
         break;
      case '1':
         str_res = "THOST_FTDC_PSRC_Buy:买委托价";
         break;
      case '2':
         str_res = "THOST_FTDC_PSRC_Sell:卖委托价";
         break;
      case '3':
         str_res = "THOST_FTDC_PSRC_OTC:场外成交价";
         break;
      default:
         str_res = "Unknown PriceSource";
         break;
     }
   return str_res;
  }
// 成交来源
char CDealInfo::TradeSource(void) const
  {
   return(m_deal.TradeSource);
  }
// 成交来源注释
string CDealInfo::TradeSourceDescription(void) const
  {
   string str_res;
   switch(m_deal.TradeSource)
     {
      case '0':
         str_res = "THOST_FTDC_TSRC_NORMAL:来自交易所普通回报";
         break;
      case '1':
         str_res = "THOST_FTDC_TSRC_QUERY:来自查询";
         break;
      default:
         str_res = "Unknown TradeSource";
         break;
     }
   return(str_res);
  }
// 数量
long CDealInfo::Volume(void) const
  {
   return(m_deal.Volume);
  }
// 序号
long CDealInfo::SequenceNo(void) const
  {
   return(m_deal.SequenceNo);
  }
// 结算编号
long CDealInfo::SettlementID(void) const
  {
   return(m_deal.SettlementID);
  }
// 经纪公司报单序号
long CDealInfo::BrokerOrderSeq(void) const
  {
   return(m_deal.BrokerOrderSeq);
  }
//+------------------------------------------------------------------+
//| Access functions HistoryDealInfoInteger(...)                     |
//+------------------------------------------------------------------+
bool CDealInfo::InfoInteger(CTP::ENUM_DEAL_PROPERTY_INTEGER prop_id,long &var) const
  {
   bool bool_res = false;
   switch(prop_id)
     {
      case CTP::DEAL_Direction:
         var = (long)m_deal.Direction;
         bool_res = true;
         break;
      case CTP::DEAL_TradingRole:
         var = (long)m_deal.TradingRole;
         bool_res = true;
         break;
      case CTP::DEAL_OffsetFlag:
         var = (long)m_deal.OffsetFlag;
         bool_res = true;
         break;
      case CTP::DEAL_HedgeFlag:
         var = (long)m_deal.HedgeFlag;
         bool_res = true;
         break;
      case CTP::DEAL_Volume:
         var = (long)m_deal.Volume;
         bool_res = true;
         break;
      case CTP::DEAL_TradeType:
         var = (long)m_deal.TradeType;
         bool_res = true;
         break;
      case CTP::DEAL_PriceSource:
         var = (long)m_deal.PriceSource;
         bool_res = true;
         break;
      case CTP::DEAL_SequenceNo:
         var = (long)m_deal.SequenceNo;
         bool_res = true;
         break;
      case CTP::DEAL_SettlementID:
         var = (long)m_deal.SettlementID;
         bool_res = true;
         break;
      case CTP::DEAL_BrokerOrderSeq:
         var = (long)m_deal.BrokerOrderSeq;
         bool_res = true;
         break;
      case CTP::DEAL_TradeSource:
         var = (long)m_deal.TradeSource;
         bool_res = true;
         break;
      default:
         break;
     }
   return(bool_res);
  }
//+------------------------------------------------------------------+
//| Access functions HistoryDealInfoDouble(...)                      |
//+------------------------------------------------------------------+
bool CDealInfo::InfoDouble(CTP::ENUM_DEAL_PROPERTY_DOUBLE prop_id,double &var) const
  {
   bool bool_res = false;
   switch(prop_id)
     {
      case CTP::DEAL_Price:
         var = m_deal.Price;
         bool_res = true;
         break;
      default:
         break;
     }
   return(bool_res);
  }
//+------------------------------------------------------------------+
//| Access functions HistoryDealInfoString(...)                      |
//+------------------------------------------------------------------+
bool CDealInfo::InfoString(CTP::ENUM_DEAL_PROPERTY_STRING prop_id,string &var) const
  {
   bool bool_res = false;
   switch(prop_id)
     {
      case CTP::DEAL_BrokerID :
         var = ::CharArrayToString(m_deal.BrokerID);
         bool_res = true;
         break;
      case CTP::DEAL_InvestorID :
         var = ::CharArrayToString(m_deal.InvestorID);
         bool_res = true;
         break;
      case CTP::DEAL_InstrumentID :
         var = ::CharArrayToString(m_deal.InstrumentID);
         bool_res = true;
         break;
      case CTP::DEAL_OrderRef :
         var = ::CharArrayToString(m_deal.OrderRef);
         bool_res = true;
         break;
      case CTP::DEAL_UserID :
         var = ::CharArrayToString(m_deal.UserID);
         bool_res = true;
         break;
      case CTP::DEAL_ExchangeID :
         var = ::CharArrayToString(m_deal.ExchangeID);
         bool_res = true;
         break;
      case CTP::DEAL_TradeID :
         var = ::CharArrayToString(m_deal.TradeID);
         bool_res = true;
         break;
      case CTP::DEAL_OrderSysID :
         var = ::CharArrayToString(m_deal.OrderSysID);
         bool_res = true;
         break;
      case CTP::DEAL_ParticipantID :
         var = ::CharArrayToString(m_deal.ParticipantID);
         bool_res = true;
         break;
      case CTP::DEAL_ClientID :
         var = ::CharArrayToString(m_deal.ClientID);
         bool_res = true;
         break;
      case CTP::DEAL_ExchangeInstID :
         var = ::CharArrayToString(m_deal.ExchangeInstID);
         bool_res = true;
         break;
      case CTP::DEAL_TradeDate :
         var = ::CharArrayToString(m_deal.TradeDate);
         bool_res = true;
         break;
      case CTP::DEAL_TradeTime :
         var = ::CharArrayToString(m_deal.TradeTime);
         bool_res = true;
         break;
      case CTP::DEAL_TraderID :
         var = ::CharArrayToString(m_deal.TraderID);
         bool_res = true;
         break;
      case CTP::DEAL_OrderLocalID :
         var = ::CharArrayToString(m_deal.OrderLocalID);
         bool_res = true;
         break;
      case CTP::DEAL_ClearingPartID :
         var = ::CharArrayToString(m_deal.ClearingPartID);
         bool_res = true;
         break;
      case CTP::DEAL_BusinessUnit :
         var = ::CharArrayToString(m_deal.BusinessUnit);
         bool_res = true;
         break;
      case CTP::DEAL_TradingDay :
         var = ::CharArrayToString(m_deal.TradingDay);
         bool_res = true;
         break;
      case CTP::DEAL_InvestUnitID :
         var = ::CharArrayToString(m_deal.InvestUnitID);
         bool_res = true;
         break;
      default:
         break;
     }
   return(bool_res);
  }
//+------------------------------------------------------------------+
//| method for select deal                                           |
//+------------------------------------------------------------------+
bool CDealInfo::Select(const string ticket)
  {
   ::ZeroMemory(m_deal);
   if(mt5ctp::HistoryDealSelectByTicket(ticket,m_deal))
     {
      m_ticket = ticket;
      return(true);
     }
   return(false);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool CDealInfo::SelectByIndex(const int index)
  {
   string ticket= CTP::HistoryDealGetTicket(index);
   return(Select(ticket));
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
